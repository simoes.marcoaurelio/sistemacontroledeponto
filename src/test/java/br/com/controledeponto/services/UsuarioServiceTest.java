package br.com.controledeponto.services;

import br.com.controledeponto.dtos.ConsultaDePontoDTO;
import br.com.controledeponto.dtos.RetornoDoRegistroDePontoDTO;
import br.com.controledeponto.dtos.UsuarioDTO;
import br.com.controledeponto.models.RegistroDePonto;
import br.com.controledeponto.models.Usuario;
import br.com.controledeponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;
    Usuario usuario2;
    Usuario usuario3;
    Usuario usuario4;
    Usuario usuario5;

    RegistroDePonto registroDePonto4;
    List<RegistroDePonto> listaDeRegistros4;

    RegistroDePonto registroDePonto5;
    List<RegistroDePonto> listaDeRegistros5;

    @BeforeEach
    public void setup() {

        usuario = new Usuario();
        usuario.setNomeCompleto("Usuário Teste");
        usuario.setCpf("596.671.460-65");
        usuario.setEmail("emailteste@email.com");

        usuario2 = new Usuario();
        usuario2.setNomeCompleto("Usuário Teste2");
        usuario2.setCpf("453.465.660-23");
        usuario2.setEmail("emailteste2@email.com");

        usuario3 = new Usuario();
        usuario3.setIdUsuario(3);
        usuario3.setNomeCompleto("Usuário Teste3");
        usuario3.setCpf("735.385.690-45");
        usuario3.setEmail("emailteste3@email.com");
        usuario3.setDataDeCadastro(LocalDate.now());


        usuario4 = new Usuario();
        usuario4.setIdUsuario(4);
        usuario4.setNomeCompleto("Usuário Teste4");
        usuario4.setCpf("735.385.690-45");
        usuario4.setEmail("emailteste4@email.com");
        usuario4.setDataDeCadastro(LocalDate.now());

        registroDePonto4 = new RegistroDePonto();
        listaDeRegistros4 = new ArrayList<>();

        registroDePonto4.setIdRegistro(1);
        registroDePonto4.setDataDoRegistro(LocalDate.now());
        registroDePonto4.setHorarioDeEntrada(LocalTime.now());
        registroDePonto4.setStatusApontamento("A");
        listaDeRegistros4.add(registroDePonto4);

        usuario4.setRegistrosDePonto(listaDeRegistros4);


        usuario5 = new Usuario();
        usuario5.setIdUsuario(5);
        usuario5.setNomeCompleto("Usuário Teste5");
        usuario5.setCpf("142.792.250-01");
        usuario5.setEmail("emailteste5@email.com");
        usuario5.setDataDeCadastro(LocalDate.now());

        registroDePonto5 = new RegistroDePonto();
        listaDeRegistros5 = new ArrayList<>();

        registroDePonto5.setIdRegistro(2);
        registroDePonto5.setDataDoRegistro(LocalDate.now());
        registroDePonto5.setHorarioDeEntrada(LocalTime.parse("18:00:00"));
        registroDePonto5.setHorarioDeSaida(LocalTime.parse("19:00:00"));
        registroDePonto5.setStatusApontamento("F");
        listaDeRegistros5.add(registroDePonto5);

        usuario5.setRegistrosDePonto(listaDeRegistros5);
    }

    @Test
    public void testarCadastrarNovoUsuario() {

        Optional<Usuario> optionalUsuario = Optional.empty();
        Mockito.when(usuarioRepository.findByCpf(Mockito.anyString())).thenReturn(optionalUsuario);
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);

        UsuarioDTO usuarioDTO = usuarioService.cadastrarNovoUsuario(usuario);

        Assertions.assertEquals(usuarioDTO.getCpf(), usuario.getCpf());
    }

    @Test
    public void testarCadastrarUsuarioJaExistente() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario);
        Mockito.when(usuarioRepository.findByCpf(Mockito.anyString())).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.cadastrarNovoUsuario(usuario);
        });
    }

    @Test
    public void testarBuscarTodosOsUsuarios() {

        Iterable<Usuario> usuarios = Arrays.asList(usuario, usuario2);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        List<UsuarioDTO> listUsuarios = usuarioService.buscarTodosOsUsuarios();

        Assertions.assertEquals(listUsuarios.size(), 2);
    }

    @Test
    public void testarBuscarTodosOsUsuariosVazio() {

        Iterable<Usuario> usuarios = Arrays.asList();
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.buscarTodosOsUsuarios();
        });
    }

    @Test
    public void testarBuscarUsuarioPorId() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(optionalUsuario);

        UsuarioDTO usuarioDTO = usuarioService.buscarUsuarioPorId(usuario.getIdUsuario());

        Assertions.assertEquals(usuarioDTO.getCpf(), usuario.getCpf());
    }

    @Test
    public void testarBuscarUsuarioPorIdInexistente() {

        Optional<Usuario> optionalUsuario = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.buscarUsuarioPorId(1234);
        });
    }

    @Test
    public void testarBuscarUsuarioPorCpfJaCadastrado() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario);
        Mockito.when(usuarioRepository.findByCpf(Mockito.anyString())).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.buscarUsuarioPorCpfJaCadastrado(usuario.getCpf());
        });
    }

    @Test
    public void testarBuscarUsuarioPorCpfNaoCadastrado() {

        Optional<Usuario> optionalUsuario = Optional.empty();
        Mockito.when(usuarioRepository.findByCpf(Mockito.anyString())).thenReturn(optionalUsuario);

        boolean usuarioJaCadastrado = usuarioService.buscarUsuarioPorCpfJaCadastrado(usuario.getCpf());

        Assertions.assertEquals(usuarioJaCadastrado, false);
    }

    @Test
    public void testarAtualizarUsuarioPorId() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(1)).thenReturn(optionalUsuario);

        usuario.setIdUsuario(1);

        UsuarioDTO usuarioDTO = usuarioService.atualizarUsuarioPorId(1, usuario);

        Assertions.assertEquals(usuarioDTO.getIdUsuario(), 1);
    }

    @Test
    public void testarAtualizarUsuarioPorIdInexistente() {

        Optional<Usuario> optionalUsuario = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.atualizarUsuarioPorId(1, usuario);
        });
    }

    @Test
    public void testarDeletarUsuarioPorId() {

        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(true);
        usuarioService.deletarUsuarioPorId(usuario.getIdUsuario());

        Mockito.verify(usuarioRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarDeletarUsuarioPorIdInexistente() {

        Mockito.when(usuarioRepository.existsById(Mockito.anyInt())).thenReturn(false);
        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.deletarUsuarioPorId(1234);});
    }

    @Test
    public void testarIncluirRegistroDePontoComTipoRegistroInvalido() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario3);
        Mockito.when(usuarioRepository.findById(3)).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO =
                    usuarioService.incluirRegistroDePonto(3, "INVALIDO");
        });
    }

    @Test
    public void testarIncluirRegistroDePontoSemUsuario() {

        Optional<Usuario> optionalUsuario = Optional.empty();
        Mockito.when(usuarioRepository.findById(1234)).thenReturn(optionalUsuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO =
                    usuarioService.incluirRegistroDePonto(1234, "ENTRADA");
        });
    }

    @Test
    public void testarIncluirRegistroDePontoEntrada() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario3);
        Mockito.when(usuarioRepository.findById(3)).thenReturn(optionalUsuario);

        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario3);

        RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO =
                usuarioService.incluirRegistroDePonto(3, "ENTRADA");

        Assertions.assertEquals(retornoDoRegistroDePontoDTO.getDataDeRegistro(), LocalDate.now());
    }

    @Test
    public void testarIncluirRegistroDePontoSaida() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario4);
        Mockito.when(usuarioRepository.findById(4)).thenReturn(optionalUsuario);

        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario4);

        RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO =
                usuarioService.incluirRegistroDePonto(4, "SAIDA");

        Assertions.assertEquals(retornoDoRegistroDePontoDTO.getDataDeRegistro(), LocalDate.now());
    }

    @Test
    public void testarConsultarInformacaoesDePonto() {

        Optional<Usuario> optionalUsuario = Optional.of(usuario5);
        Mockito.when(usuarioRepository.findById(5)).thenReturn(optionalUsuario);

        ConsultaDePontoDTO consultaDePontoDTO = usuarioService.consultarInformacoesDePonto(5);

        Assertions.assertEquals(consultaDePontoDTO.getIdUsuario(), usuario5.getIdUsuario());
    }
}
