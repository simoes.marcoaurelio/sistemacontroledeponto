package br.com.controledeponto.controllers;

import br.com.controledeponto.dtos.ConsultaDePontoDTO;
import br.com.controledeponto.dtos.RetornoDoRegistroDePontoDTO;
import br.com.controledeponto.dtos.UsuarioDTO;
import br.com.controledeponto.models.RegistroDePonto;
import br.com.controledeponto.models.Usuario;
import br.com.controledeponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    UsuarioDTO usuarioDTO;
    UsuarioDTO usuarioDTO2;

    RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO;
    ConsultaDePontoDTO consultaDePontoDTO;
    RegistroDePonto registroDePonto;
    List<RegistroDePonto> listaDeRegistros;

    @BeforeEach
    public void setup() {

        usuario = new Usuario();
        usuario.setNomeCompleto("Nome Teste");
        usuario.setCpf("107.109.670-20");
        usuario.setEmail("emailteste@email.com");

        usuarioDTO = new UsuarioDTO();
        usuarioDTO.setNomeCompleto("Nome Teste");
        usuarioDTO.setCpf("107.109.670-20");
        usuarioDTO.setEmail("emailteste@email.com");

        usuarioDTO2 = new UsuarioDTO();
        usuarioDTO2.setIdUsuario(1);
        usuarioDTO2.setNomeCompleto("Nome Teste2");
        usuarioDTO2.setCpf("107.109.670-20");
        usuarioDTO2.setEmail("emailteste2@email.com");
        usuarioDTO2.setDataDeCadastro(LocalDate.now());

        retornoDoRegistroDePontoDTO = new RetornoDoRegistroDePontoDTO();
        retornoDoRegistroDePontoDTO.setMensagemAoUsuario("Incluído com sucesso");
        retornoDoRegistroDePontoDTO.setHaviaPendencia(false);
        retornoDoRegistroDePontoDTO.setDataDeRegistro(LocalDate.now());
        retornoDoRegistroDePontoDTO.setHorarioDoRegistro(LocalTime.now());


        consultaDePontoDTO = new ConsultaDePontoDTO();
        consultaDePontoDTO.setIdUsuario(1);
        consultaDePontoDTO.setNomeCompleto("Teste Consulta");
        consultaDePontoDTO.setTotalDeHorasTrabalhadas(LocalTime.parse("10:00:00"));

        registroDePonto = new RegistroDePonto();
        listaDeRegistros = new ArrayList<>();

        registroDePonto.setIdRegistro(1);
        registroDePonto.setDataDoRegistro(LocalDate.now());
        registroDePonto.setHorarioDeEntrada(LocalTime.parse("18:00:00"));
        registroDePonto.setHorarioDeSaida(LocalTime.parse("19:00:00"));
        registroDePonto.setStatusApontamento("F");
        listaDeRegistros.add(registroDePonto);

        consultaDePontoDTO.setListaDeRegistrosFinalizados(listaDeRegistros);
    }

    @Test
    public void testarCadastrarNovoUsuario() throws Exception {

        Mockito.when(usuarioService.cadastrarNovoUsuario(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
            usuarioDTO.setIdUsuario(1);
            usuarioDTO.setDataDeCadastro(LocalDate.now());
            return usuarioDTO;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.idUsuario", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarCadastrarNovoUsuarioExistente() throws Exception {

        Mockito.when(usuarioService.cadastrarNovoUsuario(
                Mockito.any(Usuario.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarBuscarTodosOsUsuarios() throws Exception {

        List<UsuarioDTO> listaUsuarios = Arrays.asList(usuarioDTO, usuarioDTO2);
        Mockito.when(usuarioService.buscarTodosOsUsuarios()).thenReturn(listaUsuarios);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isFound());
    }

    @Test
    public void testarBuscarTodosOsUsuariosVazio() throws Exception {

        Mockito.when(usuarioService.buscarTodosOsUsuarios()).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testarBuscarUsuarioPorId() throws Exception {

        Mockito.when(usuarioService.buscarUsuarioPorId(Mockito.anyInt())).thenReturn(usuarioDTO2);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO2);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.idUsuario", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarUsuarioPorIdInexistente() throws Exception {

        Mockito.when(usuarioService.buscarUsuarioPorId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO2);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testarAtualizarUsuarioPorId() throws Exception {

        Mockito.when(usuarioService.atualizarUsuarioPorId(1, usuario)).thenReturn(usuarioDTO2);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO2);
        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testarAtualizarUsuarioPorIdInexistente() throws Exception {

        Mockito.when(usuarioService.atualizarUsuarioPorId(Mockito.anyInt(), Mockito.any(Usuario.class)))
                .thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(usuarioDTO2);
        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1234")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testarIncluirRegistrodDeEntrada() throws Exception {

        Mockito.when(usuarioService.incluirRegistroDePonto(1, "ENTRADA"))
                .thenReturn(retornoDoRegistroDePontoDTO);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(retornoDoRegistroDePontoDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/registros/1?tipoRegistro=ENTRADA")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarIncluirRegistrodDeSaida() throws Exception {

        Mockito.when(usuarioService.incluirRegistroDePonto(1, "SAIDA"))
                .thenReturn(retornoDoRegistroDePontoDTO);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(retornoDoRegistroDePontoDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/registros/1?tipoRegistro=SAIDA")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testarIncluirRegistroComTipoInvalido() throws Exception {

        Mockito.when(usuarioService.incluirRegistroDePonto(1, "INVALIDO"))
                .thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(retornoDoRegistroDePontoDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/registros/1?tipoRegistro=INVALIDO")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testarIncluirRegistroDeUsuarioInexistente() throws Exception {

        Mockito.when(usuarioService.incluirRegistroDePonto(12, "ENTRADA"))
                .thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(retornoDoRegistroDePontoDTO);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios/registros/12?tipoRegistro=ENTRADA")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testarConsultarInformacoesDePontoDeUsuarioInexistente() throws Exception {

        Mockito.when(usuarioService.consultarInformacoesDePonto(12)).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonUsuario = mapper.writeValueAsString(retornoDoRegistroDePontoDTO);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/consultas/12")
                .contentType(MediaType.APPLICATION_JSON).content(jsonUsuario))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
