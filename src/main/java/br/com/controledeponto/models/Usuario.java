package br.com.controledeponto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
@JsonIgnoreProperties(value = {"dataDeCadastro"}, allowGetters = true)
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idUsuario;

    @NotBlank(message = "Insira seu nome completo para o cadastro")
    @Size(min = 2, max = 100, message = "O nome cadastrado deve ter entre 2 e 100 caracteres")
    private String nomeCompleto;

    @CPF(message = "Digite seu CPF corretamente para o cadastro")
    @Column(unique = true)
    private String cpf;

    @NotBlank(message = "Insira um email válido para o cadastro")
    @Email(message = "Digite seu email corretamente para o cadastro")
    private String email;

    private LocalDate dataDeCadastro;

    @OneToMany(cascade = CascadeType.ALL)
    private List<RegistroDePonto> registrosDePonto;


    // CONSTRUTOR VAZIO

    public Usuario() {
    }

    // GETTERS & SETTERS

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataDeCadastro() {
        return dataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        this.dataDeCadastro = dataDeCadastro;
    }

    public List<RegistroDePonto> getRegistrosDePonto() {
        return registrosDePonto;
    }

    public void setRegistrosDePonto(List<RegistroDePonto> registrosDePonto) {
        this.registrosDePonto = registrosDePonto;
    }
}
