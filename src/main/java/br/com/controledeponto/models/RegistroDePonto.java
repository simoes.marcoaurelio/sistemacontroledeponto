package br.com.controledeponto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@JsonIgnoreProperties(value = {"dataDoRegistro", "horarioDeEntrada", "horarioDeSaida", "statusApontamento"},
        allowGetters = true)

public class RegistroDePonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idRegistro;
    private LocalDate dataDoRegistro;
    private LocalTime horarioDeEntrada;
    private LocalTime horarioDeSaida;
    private String statusApontamento;

    public RegistroDePonto() {
    }

    public int getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(int idRegistro) {
        this.idRegistro = idRegistro;
    }

    public LocalDate getDataDoRegistro() {
        return dataDoRegistro;
    }

    public void setDataDoRegistro(LocalDate dataDoRegistro) {
        this.dataDoRegistro = dataDoRegistro;
    }

    public LocalTime getHorarioDeEntrada() {
        return horarioDeEntrada;
    }

    public void setHorarioDeEntrada(LocalTime horarioDeEntrada) {
        this.horarioDeEntrada = horarioDeEntrada;
    }

    public LocalTime getHorarioDeSaida() {
        return horarioDeSaida;
    }

    public void setHorarioDeSaida(LocalTime horarioDeSaida) {
        this.horarioDeSaida = horarioDeSaida;
    }

    public String getStatusApontamento() {
        return statusApontamento;
    }

    public void setStatusApontamento(String statusApontamento) {
        this.statusApontamento = statusApontamento;
    }
}
