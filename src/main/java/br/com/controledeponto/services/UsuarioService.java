package br.com.controledeponto.services;

import br.com.controledeponto.dtos.ConsultaDePontoDTO;
import br.com.controledeponto.dtos.RetornoDoRegistroDePontoDTO;
import br.com.controledeponto.dtos.UsuarioDTO;
import br.com.controledeponto.models.RegistroDePonto;
import br.com.controledeponto.models.Usuario;
import br.com.controledeponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    // CADASTRAR NO USUARIO
    public UsuarioDTO cadastrarNovoUsuario(Usuario usuario) {

        boolean usuarioJaCadastrado = buscarUsuarioPorCpfJaCadastrado(usuario.getCpf());

        LocalDate data = LocalDate.now();
        usuario.setDataDeCadastro(data);

        usuarioRepository.save(usuario);

        UsuarioDTO usuarioDTO = new UsuarioDTO();
        movimentaUsuarioParaUsuariodto(usuario, usuarioDTO);

        return usuarioDTO;
    }

    // BUSCAR TODOS OS USUARIOS CADASTRADOS
    public List<UsuarioDTO> buscarTodosOsUsuarios() {

        Iterable<Usuario> usuarios = usuarioRepository.findAll();

        List<UsuarioDTO> listaUsuarios = new ArrayList<>();

        UsuarioDTO usuarioDTO;

        int contador = 0;
        for (Usuario usuario : usuarios) {
            contador ++;

            usuarioDTO = new UsuarioDTO();

            movimentaUsuarioParaUsuariodto(usuario, usuarioDTO);

            listaUsuarios.add(usuarioDTO);
        }

        if (contador == 0) {
            throw new RuntimeException("Não há usuários cadastrados no sistema");
        }

        return listaUsuarios;
    }

    // BUSCAR USUARIO POR ID
    public UsuarioDTO buscarUsuarioPorId(int idUsuario) {

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);

        if (optionalUsuario.isPresent()) {

            Usuario usuario = optionalUsuario.get();
            UsuarioDTO usuarioDTO = new UsuarioDTO();

            movimentaUsuarioParaUsuariodto(usuario, usuarioDTO);

            return usuarioDTO;
        }
        else {
            throw new  RuntimeException("O usuário informado não existe");
        }
    }

    // VALIDAR SE O CPF JA EXISTE NA BASE ANTES DO CADASTRO
    public boolean buscarUsuarioPorCpfJaCadastrado(String cpf) {

        Optional<Usuario> optionalUsuario = usuarioRepository.findByCpf(cpf);

        if (optionalUsuario.isPresent()) {
            throw new RuntimeException("Usuário já cadastrado no sistema, um novo cadastro não foi efetuado");
        }
        return false;
    }

    // ATUALIZAR CADASTRO DE USUARIO
    public UsuarioDTO atualizarUsuarioPorId(int idUsuario, Usuario usuario) {

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);

        if (optionalUsuario.isPresent()) {
            Usuario usuarioObjeto = optionalUsuario.get();

            usuarioObjeto.setNomeCompleto(usuario.getNomeCompleto());
            usuarioObjeto.setCpf(usuario.getCpf());
            usuarioObjeto.setEmail(usuario.getEmail());

            usuarioRepository.save(usuarioObjeto);

            UsuarioDTO usuarioDTO = new UsuarioDTO();
            movimentaUsuarioParaUsuariodto(usuarioObjeto, usuarioDTO);

            return usuarioDTO;
        }
        else {
            throw new RuntimeException("O usuário informado não existe");
        }
    }

    // DELETAR USUARIO POR ID
    public void deletarUsuarioPorId(int idUsuario) {

        if (usuarioRepository.existsById(idUsuario)) {
            usuarioRepository.deleteById(idUsuario);
        }
        else {
            throw new RuntimeException("Usuário informado não encontrado");
        }
    }

    // MOVIMENTAR CAMPOS DA CLASSE USUARIO PARA A CLASSE USUARIODTO
    public UsuarioDTO movimentaUsuarioParaUsuariodto(Usuario usuario, UsuarioDTO usuarioDTO) {

        usuarioDTO.setIdUsuario(usuario.getIdUsuario());
        usuarioDTO.setNomeCompleto(usuario.getNomeCompleto());
        usuarioDTO.setCpf(usuario.getCpf());
        usuarioDTO.setEmail(usuario.getEmail());
        usuarioDTO.setDataDeCadastro(usuario.getDataDeCadastro());

        return usuarioDTO;
    }

    // INCLUIR MARCAÇÃO DE PONTO
    public RetornoDoRegistroDePontoDTO incluirRegistroDePonto(int idUsuario, String tipoRegistro) {

        RegistroDePonto registroDePonto = new RegistroDePonto();
        RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO = new RetornoDoRegistroDePontoDTO();
        List<RegistroDePonto> listaDeRegistros = new ArrayList<>();

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);

        if (optionalUsuario.isPresent()) {
            Usuario usuarioObjeto = optionalUsuario.get();

//            if (usuarioObjeto.getRegistrosDePonto() == null)
//                retornoDoRegistroDePontoDTO.setMensagemAoUsuario("não enxergou registros");
//            else {
//                retornoDoRegistroDePontoDTO.setMensagemAoUsuario("enxergou registros anteriores");
//            }

            if (tipoRegistro.equals("ENTRADA")) {
                listaDeRegistros = usuarioObjeto.getRegistrosDePonto();

                registroDePonto.setDataDoRegistro(LocalDate.now());
                registroDePonto.setHorarioDeEntrada(LocalTime.now());
                registroDePonto.setStatusApontamento("A");
                listaDeRegistros.add(registroDePonto);

                usuarioObjeto.setRegistrosDePonto(listaDeRegistros);

                usuarioRepository.save(usuarioObjeto);
                retornoDoRegistroDePontoDTO.setMensagemAoUsuario("Registro de entrada efetuado com sucesso!");
                retornoDoRegistroDePontoDTO.setHaviaPendencia(false);
                retornoDoRegistroDePontoDTO.setDataDeRegistro(registroDePonto.getDataDoRegistro());
                retornoDoRegistroDePontoDTO.setHorarioDoRegistro(registroDePonto.getHorarioDeEntrada());
            }
            else {
                if (tipoRegistro.equals("SAIDA")) {

                    List<RegistroDePonto> listaDeRegistrosAnteriores = new ArrayList<>();
                    listaDeRegistrosAnteriores = usuarioObjeto.getRegistrosDePonto();

                    for (RegistroDePonto registroAnterior : listaDeRegistrosAnteriores) {
                        if (registroAnterior.getHorarioDeSaida() == null) {
                            registroAnterior.setHorarioDeSaida(LocalTime.now());
                            registroAnterior.setStatusApontamento("F");
                            retornoDoRegistroDePontoDTO.setMensagemAoUsuario("Registro de saída efetuado com sucesso!");
                            retornoDoRegistroDePontoDTO.setHaviaPendencia(false);
                            retornoDoRegistroDePontoDTO.setDataDeRegistro(registroAnterior.getDataDoRegistro());
                            retornoDoRegistroDePontoDTO.setHorarioDoRegistro(registroAnterior.getHorarioDeSaida());
                        }
                    }
                    usuarioRepository.save(usuarioObjeto);
                }
                else {
                    throw new RuntimeException("Tipo de registro inválido");
                }
            }
            return retornoDoRegistroDePontoDTO;
        }
        else {
            throw new RuntimeException("Usuário não encontrado");
        }
    }

    // CONSULTAR REGISTROS DE PONTO E TOTAL DE HORAS TRABALHADAS POR ID
    public ConsultaDePontoDTO consultarInformacoesDePonto(int idUsuario) {

        ConsultaDePontoDTO consultaDePontoDTO = new ConsultaDePontoDTO();

        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);

        if (optionalUsuario.isPresent()) {
            Usuario usuarioObjeto = optionalUsuario.get();

            List<RegistroDePonto> listaDeRegistrosDoUsuario = new ArrayList<>();
            listaDeRegistrosDoUsuario = usuarioObjeto.getRegistrosDePonto();

            List<RegistroDePonto> listaDeRegistrosFinalizados = new ArrayList<>();

            String constanteF = "F";
            consultaDePontoDTO.setTotalDeHorasTrabalhadas(LocalTime.parse("00:00:00"));
//            LocalTime totalDeHorasTrabalhadas = LocalTime.parse("00:00:00");

            for (RegistroDePonto registroDePonto : listaDeRegistrosDoUsuario) {
                if (registroDePonto.getStatusApontamento().equals(constanteF)) {
                    listaDeRegistrosFinalizados.add(registroDePonto);

                    consultaDePontoDTO.setTotalDeHorasTrabalhadas(
                            calcularHorasTrabalhadas(registroDePonto, consultaDePontoDTO.getTotalDeHorasTrabalhadas()));
                }
            }

            if (listaDeRegistrosFinalizados.size() == 0) {
                throw new RuntimeException("Não existem registros de ponto para esse usuário");
            }

            consultaDePontoDTO.setIdUsuario(usuarioObjeto.getIdUsuario());
            consultaDePontoDTO.setNomeCompleto(usuarioObjeto.getNomeCompleto());
            consultaDePontoDTO.setListaDeRegistrosFinalizados(listaDeRegistrosFinalizados);

            return consultaDePontoDTO;
        }
        else {
            throw new RuntimeException("Usuário não encontrado");
        }
    }

    public LocalTime calcularHorasTrabalhadas(RegistroDePonto registroDePonto, LocalTime totalDeHorasTrabalhadas) {

        int horaEntrada = registroDePonto.getHorarioDeEntrada().getHour();
        int minutoEntrada = registroDePonto.getHorarioDeEntrada().getMinute();
        int segundoEntrada = registroDePonto.getHorarioDeEntrada().getSecond();

        LocalTime horasTrabalhadasNoDia = registroDePonto.getHorarioDeSaida()
                .minusHours(horaEntrada)
                .minusMinutes(minutoEntrada)
                .minusSeconds(segundoEntrada);

        int horaTrabalhada = horasTrabalhadasNoDia.getHour();
        int minutoTrabalhado = horasTrabalhadasNoDia.getMinute();
        int segundoTrabalhado = horasTrabalhadasNoDia.getSecond();

        totalDeHorasTrabalhadas = totalDeHorasTrabalhadas
                .plusHours(horaTrabalhada)
                .plusMinutes(minutoTrabalhado)
                .plusSeconds(segundoTrabalhado);

        return totalDeHorasTrabalhadas;
    }
}
