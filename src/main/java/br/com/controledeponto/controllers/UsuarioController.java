package br.com.controledeponto.controllers;

import br.com.controledeponto.dtos.ConsultaDePontoDTO;
import br.com.controledeponto.dtos.RetornoDoRegistroDePontoDTO;
import br.com.controledeponto.dtos.UsuarioDTO;
import br.com.controledeponto.models.Usuario;
import br.com.controledeponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    // CADASTRAR NOVO USUARIO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UsuarioDTO cadastrarNovoUsuario(@RequestBody @Valid Usuario usuario) {
        try {
            return usuarioService.cadastrarNovoUsuario(usuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    // CONSULTAR TODOS OS USUARIOS CADASTRADOS
    @GetMapping
    @ResponseStatus(HttpStatus.FOUND)
    public List<UsuarioDTO> buscarTodosOsUsuarios() {
        try {
            return usuarioService.buscarTodosOsUsuarios();
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    // CONSULTAR USUARIO POR ID
    @GetMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.FOUND)
    public UsuarioDTO buscarUsuarioPorId(@PathVariable int idUsuario) {
        try {
            UsuarioDTO usuarioDTO = usuarioService.buscarUsuarioPorId(idUsuario);
            return usuarioDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    // ATUALIZAR USUARIO POR ID
    @PutMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.OK)
    public UsuarioDTO atualizarUsuarioPorId(@PathVariable int idUsuario, @RequestBody @Valid Usuario usuario) {
        try {
            UsuarioDTO usuarioDTO = usuarioService.atualizarUsuarioPorId(idUsuario, usuario);
            return usuarioDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    // DELETAR USUARIO POR ID
    @DeleteMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarUsuarioPorId(@PathVariable int idUsuario) {
        try {
            usuarioService.deletarUsuarioPorId(idUsuario);
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    // INCLUIR REGISTRO DE PONTO
    @PostMapping("/registros/{idUsuario}")
    @ResponseStatus(HttpStatus.CREATED)
    public RetornoDoRegistroDePontoDTO incluirRegistroDeEntrada(
            @PathVariable int idUsuario,
            @RequestParam(name = "tipoRegistro", required = true) String tipoRegistro) {

        try {
            RetornoDoRegistroDePontoDTO retornoDoRegistroDePontoDTO =
                    usuarioService.incluirRegistroDePonto(idUsuario, tipoRegistro);
            return retornoDoRegistroDePontoDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    // CONSULTAR REGISTROS DE PONTO DE UM DETERMINADO USUÁRIO
    @GetMapping("/consultas/{idUsuario}")
    @ResponseStatus(HttpStatus.FOUND)
    public ConsultaDePontoDTO consultarInformacoesDePonto(@PathVariable int idUsuario) {
        try {
            ConsultaDePontoDTO consultaDePontoDTO = usuarioService.consultarInformacoesDePonto(idUsuario);
            return consultaDePontoDTO;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }
}
