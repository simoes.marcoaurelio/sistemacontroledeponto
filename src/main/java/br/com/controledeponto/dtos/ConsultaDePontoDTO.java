package br.com.controledeponto.dtos;

import br.com.controledeponto.models.RegistroDePonto;

import java.time.LocalTime;
import java.util.List;

public class ConsultaDePontoDTO {

    private int idUsuario;
    private String nomeCompleto;
    private LocalTime totalDeHorasTrabalhadas;
    private List<RegistroDePonto> listaDeRegistrosFinalizados;

    public ConsultaDePontoDTO() {
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public LocalTime getTotalDeHorasTrabalhadas() {
        return totalDeHorasTrabalhadas;
    }

    public void setTotalDeHorasTrabalhadas(LocalTime totalDeHorasTrabalhadas) {
        this.totalDeHorasTrabalhadas = totalDeHorasTrabalhadas;
    }

    public List<RegistroDePonto> getListaDeRegistrosFinalizados() {
        return listaDeRegistrosFinalizados;
    }

    public void setListaDeRegistrosFinalizados(List<RegistroDePonto> listaDeRegistrosFinalizados) {
        this.listaDeRegistrosFinalizados = listaDeRegistrosFinalizados;
    }
}
