package br.com.controledeponto.dtos;

import java.time.LocalDate;
import java.time.LocalTime;

public class RetornoDoRegistroDePontoDTO {

    private String mensagemAoUsuario;
    private boolean haviaPendencia;
    private LocalDate dataDeRegistro;
    private LocalTime horarioDoRegistro;

    public RetornoDoRegistroDePontoDTO() {
    }

    public String getMensagemAoUsuario() {
        return mensagemAoUsuario;
    }

    public void setMensagemAoUsuario(String mensagemAoUsuario) {
        this.mensagemAoUsuario = mensagemAoUsuario;
    }

    public boolean isHaviaPendencia() {
        return haviaPendencia;
    }

    public void setHaviaPendencia(boolean haviaPendencia) {
        this.haviaPendencia = haviaPendencia;
    }

    public LocalDate getDataDeRegistro() {
        return dataDeRegistro;
    }

    public void setDataDeRegistro(LocalDate dataDeRegistro) {
        this.dataDeRegistro = dataDeRegistro;
    }

    public LocalTime getHorarioDoRegistro() {
        return horarioDoRegistro;
    }

    public void setHorarioDoRegistro(LocalTime horarioDoRegistro) {
        this.horarioDoRegistro = horarioDoRegistro;
    }
}
