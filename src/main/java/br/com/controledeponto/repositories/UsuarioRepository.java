package br.com.controledeponto.repositories;

import br.com.controledeponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
    Optional<Usuario> findByCpf(String cpf);
}
