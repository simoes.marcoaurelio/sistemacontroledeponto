# sistemaControleDePonto

 Objetivo: Sistema de controle de ponto da empresa.
********************************************************************************
CONFIGURAÇÃO DO BANCO DE DADOS:

create database controle_de_ponto;
create user 'controledepontoadm'@'localhost' identified by 'senha123';
grant all on controle_de_ponto.* to 'controledepontoadm'@'localhost';
********************************************************************************
# Detalhamento dos endpoints

CADASTRO DE NOVO USUÁRIO

- Devemos informar, nome, cpf e email no corpo da requisição
- O id e a data de cadastro serão preenchidos automaticamente pelo sistema
- O primeiro registro de ponto não poderá ser cadastrado junto com o cadastro do usuário
- Nenhuma informação de registro de ponto será exibiba no retorno da ação de cadastro

Método POST: localhost:8080/usuarios
{
    "nomeCompleto": "Usuário Teste",
    "cpf": "855.039.670-20",
    "email": "emailteste@email.com"
}
********************************************************************************
BUSCAR TODOS OS USUÁRIOS CADASTRADOS

- Será retornada uma lista de usuários, sem as informações de registro de ponto (retornamos apenas informações de cadastro do usuário)
- Consulta de informações de registro de ponto deverá ser feita por uma funcionalidade exclusiva, de acordo com um determinado usuário

Método GET: localhost:8080/usuarios
********************************************************************************
BUSCAR USUÁRIO PELO ID

- De acordo com o id de usuario informado como variável na URL, serão retornadas as informações de cadastro do usuário (exceto informações de registro de ponto)
- Consulta de informações de registro de ponto deverá ser feita por uma funcionalidade exclusiva, de acordo com um determinado usuário

Método GET: localhost:8080/usuarios/{id desejado}
********************************************************************************
ATUALIZAR USUÁRIO PELO ID

- De acordo com o id de usuário informado como variável na URL, será possível atualizar as informações de nome, cpf e email de um usuário já cadastrado
- As informações de nome, cpf e email deverão ser enviadas no corpo da requisição
- Não será possível alterar id e data de cadastro, assim como qualquer informação de registro de ponto

Métoto PUT: localhost:8080/usuarios/{id desejado}
{
    "nomeCompleto": "Usuário alterado",
    "cpf": "855.039.670-20",
    "email": "emailalterado@email.com"
}
********************************************************************************
DELETAR USUÁRIO PELO ID

- De acordo com o id de usuário informado como variável na URL, será possível deletar um usuário já cadastrado

Método DELETE: localhost:8080/usuarios/{id desejado}
********************************************************************************
INCLUIR NOVO REGISTRO DE PONTO

- Para inclusão de um registro de entrada de ponto, devemos enviar a opção ENTRADA como variavel de parêmetro da URL
- Para inclusão de um registro de saída de ponto, devemos enviar a opção SAIDA como variavel de parêmetro da URL
- As informações de data e horário de registro serão preenchidas automaticamente pelo sistema

Método POST: localhost:8080/usuarios/registros/{id desejado}?tipoRegistro={ENTRADA ou SAIDA, conforme desejado}
********************************************************************************
CONSULTAR REGISTROS DE PONTO DO USUÁRIO, E SEU TOTAL DE HORAS TRABALHADAS

- Função para consulta de registros de ponto do usuário, e também a soma dtodo o período (horas) trabalhado
- De acordo com o id de usuário informado como variável na URL, o sistema retornará as informações desejadas

Método GET: localhost:8080/usuarios/consultas/{id desejado}
